package com.usm.backbaseassignment;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView recyclerView;
    ImageView imSearch, imClose;
    EditText etSearch;
    TextView tvNoData;
    ArrayList<CitiesBean> aList, searchList;
    int page;
    FrameLayout fragment_frame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        imSearch = (ImageView) findViewById(R.id.imSearch);
        imClose = (ImageView) findViewById(R.id.imClose);
        etSearch = (EditText) findViewById(R.id.etSearch);
        tvNoData = (TextView) findViewById(R.id.tvNoData);
        fragment_frame = (FrameLayout) findViewById(R.id.fragment_frame);

        // load  cities json file form assets folder
        try {
            aList = new ArrayList<>();
            JSONArray ja = new JSONArray(loadJSONFromAsset("cities.json"));
            if (ja.length() == 0) {
                recyclerView.setVisibility(View.GONE);
            } else {
                recyclerView.setVisibility(View.VISIBLE);
                for (int i = 0; i < ja.length(); i++) {
                    JSONObject obj1 = ja.getJSONObject(i);
                    CitiesBean bean1 = new CitiesBean();
                    bean1.setId(obj1.optString("id"));
                    bean1.setName(obj1.optString("name"));
                    bean1.setCounty(obj1.optString("country"));
                    bean1.setCoordination(obj1.optJSONObject("coord"));
                    aList.add(bean1);
                }
                aList = sortByAlphabetOrder(aList); // sort arraylist by alphabet order
                final GridLayoutManager manager = new GridLayoutManager(getApplicationContext(), 1);
                recyclerView.setLayoutManager(manager);
                CitiesAdapter nAdapter = new CitiesAdapter(aList);
                recyclerView.setAdapter(nAdapter);
            }

        } catch (Exception e) {
            Log.e("Exception ", e.getMessage());
            e.printStackTrace();
        }

        // search functionality
        // products search here
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etSearch.getText().toString().trim().length() > 0) {
                    tvNoData.setVisibility(View.GONE);
                    imSearch.setVisibility(View.GONE);
                    imClose.setVisibility(View.VISIBLE);
                } else {
                    imSearch.setVisibility(View.VISIBLE);
                    imClose.setVisibility(View.GONE);
                }
                // get search key matched cities
                searchList = new ArrayList<>();
                getSearchList(etSearch.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        imClose.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 1) {
            page = 0;
            fm.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Load cities files from asset folder
     *
     * @param fileName
     * @return
     */
    public String loadJSONFromAsset(String fileName) {
        String json = null;
        try {
            InputStream is = getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    /**
     * method to get search list weather it matches with name or country at starting char and case insensitive
     */
    public void getSearchList(String searchKey) {
        for (CitiesBean bean : aList) {
            if ((bean.getName() != null && bean.getName().toLowerCase().startsWith(searchKey.toLowerCase()))
                    || (bean.getCounty() != null && bean.getCounty().toLowerCase().startsWith(searchKey.toLowerCase()))) {
                CitiesBean bean1 = new CitiesBean();
                bean1.setId(bean.getId());
                bean1.setName(bean.getName());
                bean1.setCounty(bean.getCounty());
                bean1.setCoordination(bean.getCoordination());
                searchList.add(bean1);
            }
        }

        if (searchList.size() > 0) {
            tvNoData.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            searchList = sortByAlphabetOrder(searchList);
            final GridLayoutManager manager = new GridLayoutManager(getApplicationContext(), 1);
            recyclerView.setLayoutManager(manager);
            CitiesAdapter nAdapter = new CitiesAdapter(searchList);
            recyclerView.setAdapter(nAdapter);

        } else {
            tvNoData.setText("No Data Found");
            tvNoData.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    /**
     * method to sort order in alphabet
     */
    public static ArrayList<CitiesBean> sortByAlphabetOrder(ArrayList<CitiesBean> arrayList) {
        Collections.sort(arrayList, new Comparator<CitiesBean>() {
            @Override
            public int compare(CitiesBean lhs, CitiesBean rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });
        return arrayList;
    }

    public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.MyViewHolder> {
        public List<CitiesBean> settingsList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView tvName, tvCoordinates;
            RelativeLayout lyt;


            public MyViewHolder(View view) {
                super(view);
                tvName = (TextView) view.findViewById(R.id.tvName);
                tvCoordinates = (TextView) view.findViewById(R.id.tvCoordinates);
                lyt = (RelativeLayout) view.findViewById(R.id.lyt);
            }
        }

        public CitiesAdapter(List<CitiesBean> settingsList) {
            this.settingsList = settingsList;
        }

        @Override
        public CitiesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cities_adapter, parent, false);
            return new CitiesAdapter.MyViewHolder(view);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public void onBindViewHolder(final CitiesAdapter.MyViewHolder holder
                , final int position) {
            final CitiesBean bean = settingsList.get(position);
            holder.tvName.setText(bean.getName() + " - " + bean.getCounty());
            try {
                holder.tvCoordinates.setText("Lat " + bean.getCoordination().getString("lat") + " Lon " + bean.getCoordination().getString("lon"));
            } catch (Exception e) {
                e.printStackTrace();
            }


            holder.lyt.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View view) {
                    CitiesBean bean = settingsList.get(position);
                    // hide keyboard
                    InputMethodManager imm = (InputMethodManager)view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                    try {
                        Bundle data = new Bundle();
                        data.putString("lat", bean.getCoordination().getString("lat"));
                        data.putString("lon", bean.getCoordination().getString("lon"));
                        data.putString("name", bean.getName());

                        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
                        Fragment fragment = new MapFragment();
                        fragment.setArguments(data);
                        t.add(R.id.fragment_frame, fragment);
                        t.commit();
                        page = 1;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });

        }

        @Override
        public int getItemCount() {
            return settingsList.size();
        }

    }
}