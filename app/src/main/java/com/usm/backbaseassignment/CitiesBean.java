package com.usm.backbaseassignment;

import org.json.JSONObject;

public class CitiesBean {
    public String county;
    public String name;
    public String id;
    public JSONObject coordination;

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public JSONObject getCoordination() {
        return coordination;
    }

    public void setCoordination(JSONObject coordination) {
        this.coordination = coordination;
    }
}
